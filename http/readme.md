# Implantação do GitLab

**Criar diretórios de persistência de dados para o serviço**
```
mkdir -p /storage/docker-homol/deploy/gitlab/{data,logs,config}
```

**Executar o serviço**
```
docker stack deploy -c stack.yml gitlab
```

**Verificar o status dos containers no serviço**
```
docker stack ps gitlab
```

**Verificar logs**
```
docker service logs -f gitlab_gitlab
```

**resetar a senha do root GitLab**
```
gitlab-rake "gitlab:password:reset[root]"
```


**Remover o serviço**
```
docker stack rm gitlab
```

Redefinir a senha admin do grafana:
```
https://docs.gitlab.com/omnibus/settings/grafana.html
```


#### Fonte:
[gitlab docker](https://docs.gitlab.com/omnibus/docker/#install-gitlab-using-docker-compose)
[grafana](https://docs.gitlab.com/omnibus/settings/grafana.html)
